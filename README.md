# README #

### What is this repository for? ###

* Simple Classification of MNIST using a KNN like algorithm
* It's just for fun to demonstrate what else a KNN can do
* Version 0.0.1

### Drawbacks of this method ###
* It is vastly inefficient since it needs to compare all pictures everytime
* Images must be centered and not turned or skewed

### Who do I talk to? ###

* William Rogers
* wrrogers@hotmail.com