import operator
import os
import pandas as pd
import numpy as np
import requests

def knn_classify(image, trainingset, labels, k=5):
    N = trainingset.shape[0]
    baseImages = np.tile(image, (N,1))
    print("The base shape is:")
    show_image(baseImages[0])
    difference = baseImages - trainingset
    print("The first trainingset images is:")
    show_image(trainingset[0])
    print("The difference is:")
    show_image(difference[0])
    sqDifference = difference**2
    print("The squared difference is:")
    show_image(sqDifference[0])
    sqDistances = sqDifference.sum(axis=1)

    print("Squared differences for the first number:", sqDistances[0])
    distances = sqDistances**0.5 #squareroot
    print("The euclidean distance for the first one is:",distances[0])

    sorted_ind = distances.argsort()  #This sorts by index so that the order is preserved for retreiving the labels
    print("The lowest euclidean distance after sort is:", distances[sorted_ind[0]])
    print("The max euclidean distance is:", distances[sorted_ind[N-1]])
    class_count={}
    for i in range(k):
        vote_i_label = labels[sorted_ind[i]]
        print("The, ", i+1, " target number is: ", vote_i_label)
        class_count[vote_i_label] = class_count.get(vote_i_label,0) + 1 #Keeps track of all of the votes in a dictionary
    print("The counts of each vote: ")
    print(class_count)
    sorted_class_count = sorted(class_count.items(), key=operator.itemgetter(1), reverse=True) #Sort by count
    return sorted_class_count[0][0] #Return the selection with the highest count

def get_data(train_dir = "mnist_train.csv", target_dir="mnist_train_targets.csv",n = 2001):
    dataset = pd.read_csv(train_dir, header=0)
    dataset = dataset.as_matrix()
    targets = pd.read_csv(target_dir, header=0)
    targets = targets.as_matrix()
    choices = np.random.choice(len(dataset), n)
    X = np.zeros(shape=(n,784))
    y = np.zeros(shape=(n))
    i = 0
    for choice in choices:
        X[i] = dataset[choice]
        y[i] = targets[choice]
        i += 1
    return X, y

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)    

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None

def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)

def show_image(image):
    print(type(image), image.shape)
    for j in range (1,28):
        row = []
        for i in range(1,28):
            if(image[(j-1)*28+i] >= 0 and image[(j-1)*28+i] < 75):
                row+=" "
            elif(image[(j-1)*28+i] >= 75 and image[(j-1)*28+i] < 150):
                row += "."
            elif (image[(j-1)*28+i] >= 150 and image[(j-1)*28+i] < 200):
                row += "*"
            else:
                row+="#"
        print (' '.join(map(str, row)))

if 'mnist_train.csv' not in os.listdir():
    print("Downloading files to "+str(os.getcwd())+" ...")
    download_file_from_google_drive('1v_9RJ_1NOmEHK4si49jDYDLJL-_j5-8x', str(os.getcwd())+'/mnist_train_targets.csv')
    download_file_from_google_drive('1vumf_ccyCALjKC91Nnh3RUFcqp-cQiEB', str(os.getcwd())+'/mnist_train.csv')
    print("Files downloaded.")
    
X, y = get_data()
pic = X[0]
X = np.delete(X,(0),axis=0)
target = y[0]
y = np.delete(y, (0), axis=0)
error_count = 0.0
classifierResult = knn_classify(pic, X, y)

print ("The result from the classifier is:",classifierResult)
print ("The correct answer was: ", target)
show_image(pic)